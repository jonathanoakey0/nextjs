// pages/login.js

import { useState } from 'react';
import { useRouter } from 'next/router';
import { setCookie } from 'nookies';
import { generateAccessToken } from './api/auth';

export default function LoginPage() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const router = useRouter();

  function handleLogin(e) {
    e.preventDefault();

    // TODO: Validate email and password

    const userId = 1; // Replace with your user ID
    const token = generateAccessToken(userId);

    setCookie(null, 'token', token, {
      maxAge: 60 * 60, // 1 hour
      path: '/',
    });

    router.push('/');
  }

  return (
    <form onSubmit={handleLogin}>
      <label>
        Email:
        <input type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
      </label>
      <br />
      <label>
        Password:
        <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
      </label>
      <br />
      <button type="submit">Log in</button>
    </form>
  );
}
