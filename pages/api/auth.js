// utils/auth.js

const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');

const JWT_SECRET = 'your-secret-key'; // Change this to your own secret key

function generateAccessToken(userId) {
  return jwt.sign({ userId }, JWT_SECRET, { expiresIn: '1h' });
}

function authenticateUser(req, res, next) {
  const token = req.cookies.token;

  if (!token) {
    res.status(401).send('Unauthorized: No token provided');
    return;
  }

  jwt.verify(token, JWT_SECRET, (err, decoded) => {
    if (err) {
      res.status(401).send('Unauthorized: Invalid token');
      return;
    }

    req.userId = decoded.userId;
    next();
  });
}

module.exports = {
  generateAccessToken,
  authenticateUser,
};

// API/auth.js

const users = [
    { id: 1, email: 'testuser@example.com', password: 'testpassword' },
  ];
  
  export function authenticateUser(email, password) {
    return new Promise((resolve, reject) => {
      const user = users.find(u => u.email === email && u.password === password);
      if (user) {
        resolve(user.id);
      } else {
        reject(new Error('Invalid email or password'));
      }
    });
  }
  