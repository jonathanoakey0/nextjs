import { useState } from 'react';
import { setCookie } from 'nookies';
import { generateAccessToken, authenticateUser } from './api/auth';

export default function Home() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  
  function handleLogin(e) {
    e.preventDefault();

    // Authenticate user and generate access token
    authenticateUser(email, password)
      .then((userId) => {
        const token = generateAccessToken(userId);

        // Set token cookie and redirect to dashboard
        setCookie(null, 'token', token, {
          maxAge: 60 * 60, // 1 hour
          path: '/',
        });
        window.location.href = '/dashboard';
      })
      .catch((error) => {
        setErrorMessage(error.message);
      });
  }

  return (
    <div>
      <h1>Login to your account</h1>
      <form onSubmit={handleLogin}>
        <div>
          <label htmlFor="email">Email:</label>
          <input
            type="email"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </div>
        <div>
          <label htmlFor="password">Password:</label>
          <input
            type="password"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </div>
        {errorMessage && <div style={{ color: 'red' }}>{errorMessage}</div>}
        <div>
          <button type="submit">Log in</button>
        </div>
      </form>
    </div>
  );
}
